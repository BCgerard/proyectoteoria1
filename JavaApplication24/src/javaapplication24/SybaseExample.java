package javaapplication24;

import java.sql.*;
import java.util.Properties;

import javax.management.openmbean.OpenDataException;
import com.sybase.jdbc3.jdbc.SybDriver;

public class SybaseExample {

    /**
     * @param args the command line arguments
     */
    
    
    
    public static void main(String[] args) {
        try {
            Connection conexion;
            SybDriver syDriver;
            System.out.println("Probando conexion");
            
            //PASO 1, CREAR LA CONEXION
            System.out.println("Paso 1, crear la conexion");
            Class.forName("com.sybase.jdbc3.jdbc.SybDriver").newInstance();
            //lo de arriba lo podemos igualar a sydriver
            Properties properties = new Properties();
            properties.put("user", "sa");
            properties.put("password", "maximo1135");
            
            String url = "jdbc:sybase:Tds:BGGUEVARA:5001/Teoria1";
            //String url = "jdbc:sybase:Tds:5001/Teoria1";
            
            conexion = DriverManager.getConnection(url, properties);
            //retornar la conexion
            
            //PASO 2, crear el objeto tipo statement
            System.out.println("Paso 2, crear el objeto de tipo statement");
            //a partir de aca, se hacen los statements
            Statement st = conexion.createStatement();
            //PASO 3, EJECUTAR SQL
            System.out.println("Paso 3, ejecutar sql");
            ResultSet rs = st.executeQuery("SELECT Primer_Nombre, Codigo_empleado FROM Empleados"); //esto tiene forma de tabla, y tiene el resultado de la consulta
         
            //PASO 4, RECORRER EL RESULTSET
            System.out.println("Paso 4, recorrer el result set");
            while(rs.next()) {
                System.out.println(rs.getString("Codigo_empleado") +" - "+ rs.getString("Primer_Nombre"));
            }
            /*
            CON ESTO, SE OBTIENEN LOS NOMBRES DE LOS CAMPOS
            ResultSetMetaData rsmd = rs.getMetaData();
            for(int i = 0; i < rsmd.getColumnCount(); i++) {
                System.out.println(rsmd.getColumnName(i));
            }
            */
            rs.close();
            st.close();
            conexion.close();
        } catch(Exception e) {
            System.out.println("Error, no se ha conectado");
            e.printStackTrace();
        }
    }
}